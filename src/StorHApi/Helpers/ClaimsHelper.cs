﻿using IdentityModel.Client;
using StorHApi.Models.View.Auth;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StorHApi.Helpers
{
    public static class ClaimsHelper
    {
        public static ClaimsDTO GetClaims(ClaimsPrincipal principal)
        {
            var user = new ClaimsDTO();

            var claims = from c in principal.Claims
                         select new
                         {
                             type = c.Type,
                             value = c.Value
                         };

            user.skogId = (from c in claims where c.type.Equals("skogId") select c.value).FirstOrDefault();
            user.name = (from c in claims where c.type.Equals("name") select c.value).FirstOrDefault();
            user.firstname = (from c in claims where c.type.Equals("firstname") select c.value).FirstOrDefault();
            user.surname = (from c in claims where c.type.Equals("surname") select c.value).FirstOrDefault();
            user.email = (from c in claims where c.type.Equals("email") select c.value).FirstOrDefault();
            user.image = (from c in claims where c.type.Equals("image") select c.value).FirstOrDefault();
            user.applicationRole = (from c in claims where c.type.Equals("role") select c.value).FirstOrDefault();
            user.client = (from c in claims where c.type.Equals("client_id") select c.value).FirstOrDefault();

            return user;
        }

        public static async Task<ClaimsDTO> GetClaimsFromUserInfo(string accessToken)
        {
            var userInfoClient = new UserInfoClient("https://id.skogdev.no/connect/userinfo");

            var response = await userInfoClient.GetAsync(accessToken);
            var claims = response.Claims;

            var user = new ClaimsDTO();
            user.skogId = (from c in claims where c.Type.Equals("skogId") select c.Value ?? "").FirstOrDefault();
            user.name = (from c in claims where c.Type.Equals("name") select c.Value ?? "").FirstOrDefault();
            user.firstname = (from c in claims where c.Type.Equals("firstname") select c.Value ?? "").FirstOrDefault();
            user.surname = (from c in claims where c.Type.Equals("surname") select c.Value ?? "").FirstOrDefault();
            user.email = (from c in claims where c.Type.Equals("email") select c.Value ?? "").FirstOrDefault();
            user.image = (from c in claims where c.Type.Equals("image") select c.Value ?? "").FirstOrDefault();
            user.applicationRole = (from c in claims where c.Type.Equals("role") select c.Value ?? "").FirstOrDefault();

            return user;
        }

        public static ClaimsDTO DecodeToken(string accessToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var tokens = handler.ReadToken(accessToken) as JwtSecurityToken;
            
            var claims = tokens.Claims;

            var userClaims = new ClaimsDTO();
            userClaims.skogId = (from c in claims where c.Type.Equals("skogId") select c.Value ?? "").FirstOrDefault();
            userClaims.name = (from c in claims where c.Type.Equals("name") select c.Value ?? "").FirstOrDefault();
            userClaims.firstname = (from c in claims where c.Type.Equals("firstname") select c.Value ?? "").FirstOrDefault();
            userClaims.surname = (from c in claims where c.Type.Equals("surname") select c.Value ?? "").FirstOrDefault();
            userClaims.email = (from c in claims where c.Type.Equals("email") select c.Value ?? "").FirstOrDefault();
            userClaims.image = (from c in claims where c.Type.Equals("image") select c.Value ?? "").FirstOrDefault();
            userClaims.applicationRole = (from c in claims where c.Type.Equals("role") select c.Value ?? "").FirstOrDefault();
            userClaims.client = (from c in claims where c.Type.Equals("client_id") select c.Value ?? "").FirstOrDefault();

            return userClaims;
        }
    }
}
