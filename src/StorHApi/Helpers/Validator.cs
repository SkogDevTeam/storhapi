﻿using StorHApi.Models.Response;
using StorHApi.Models.Validation;
using StorHApi.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Helpers
{
    public static class Validator
    {
        public static ValidatedResult ValidatePushDevice(PushDeviceDTO device)
        {
            var returnValue = new ValidatedResult()
            {
                valid = false,
                result = new StorHApiResponse()
                {
                    statusCode = System.Net.HttpStatusCode.BadRequest,
                    message = "Invalid registration-ID",
                    result = null
                }
            };

            if (device.regId == null || device.regId.Length == 0)
                return returnValue;

            if (device.platform == null || device.platform.Length == 0)
            {
                returnValue.result.message = "Invalid platform";
                return returnValue;
            }

            if (device.uuid == null || device.uuid.Length == 0)
            {
                returnValue.result.message = "Invalid uuid";
                return returnValue;
            }

            if (device.model == null || device.model.Length == 0)
            {
                returnValue.result.message = "Invalid model";
                return returnValue;
            }

            if (device.manufacturer == null || device.manufacturer.Length == 0)
            {
                returnValue.result.message = "Invalid manufacturer";
                return returnValue;
            }

            if (device.application == null || device.application.Length == 0)
            {
                returnValue.result.message = "Invalid application";
                return returnValue;
            }

            if (device.appversion == null || device.appversion.Length == 0)
            {
                returnValue.result.message = "Invalid appversion";
                return returnValue;
            }

            returnValue.valid = true;
            return returnValue;
        }
    }
}
