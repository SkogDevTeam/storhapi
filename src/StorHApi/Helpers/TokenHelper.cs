﻿using IdentityModel.Client;
using StorHApi.Models.View.Auth;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StorHApi.Helpers
{
    public interface ITokenHelper
    {
        Task<TokenResponseDTO> requestAuthorizationCodeAsync(string tokenEndpoint, string client, string secret, string code, string redirectUri);
        Task<TokenResponseDTO> requestRefreshTokenAsync(string tokenEndPoint, string client, string secret, string token);
        Task<ClaimsDTO> getUserInfo(string userInfoEndpoint, TokenResponseDTO tokenResponse, bool fromEndpoint);
        Task<TokenResponseDTO> resourceOwnerAsync(string tokenEndPoint, string client, string secret, string username, string password, string scope);
    }

    public class TokenHelper : ITokenHelper
    {
        public async Task<TokenResponseDTO> requestAuthorizationCodeAsync(string tokenEndpoint, string client, string secret, string code, string redirectUri)
        {
            var tokenClient = new TokenClient(tokenEndpoint, client, secret);
            var response = await tokenClient.RequestAuthorizationCodeAsync(code, redirectUri);

            return new TokenResponseDTO()
            {
                accessToken = response.AccessToken,
                refreshToken = response.RefreshToken,
                identityToken = response.IdentityToken,
                isError = response.IsError,
                expiresIn = response.ExpiresIn,
            };
        }

        public async Task<TokenResponseDTO> requestRefreshTokenAsync(string tokenEndPoint, string client, string secret, string token)
        {
            var tokenClient = new TokenClient(tokenEndPoint, client, secret);
            var response = await tokenClient.RequestRefreshTokenAsync(token);

            return new TokenResponseDTO()
            {
                accessToken = response.AccessToken,
                refreshToken = response.RefreshToken,
                identityToken = response.IdentityToken ?? null,
                isError = response.IsError,
                expiresIn = response.ExpiresIn
            };
        }

        public async Task<TokenResponseDTO> resourceOwnerAsync(string tokenEndPoint, string client, string secret, string username, string password, string scope)
        {
            var tokenClient = new TokenClient(tokenEndPoint, client, secret);
            var response = await tokenClient.RequestResourceOwnerPasswordAsync(username, password, scope);

            return new TokenResponseDTO()
            {
                accessToken = response.AccessToken,
                refreshToken = response.RefreshToken,
                identityToken = response.IdentityToken ?? null,
                isError = response.IsError,
                expiresIn = response.ExpiresIn
            };

        }

        public async Task<ClaimsDTO> getUserInfo(string userInfoEndpoint, TokenResponseDTO tokenResponse, bool fromEndpoint)
        {
            var claims = new List<Claim>();
            if (fromEndpoint)
            {
                var userInfoClient = new UserInfoClient(userInfoEndpoint);
                var userInfo = await userInfoClient.GetAsync(tokenResponse.accessToken);

                if (userInfo.Claims == null || !userInfo.Claims.Any())
                {
                    throw new Exception("500");
                }
                claims = userInfo.Claims.ToList();
            }
            else
            {
                var handler = new JwtSecurityTokenHandler();
                var decrypted = handler.ReadToken(tokenResponse.identityToken) as JwtSecurityToken;

                if (decrypted.Claims == null || !decrypted.Claims.Any())
                {
                    throw new Exception("500");
                }
                claims = decrypted.Claims.ToList();
            }
            
            var customClaims = new ClaimsDTO
            {
                skogId = claims.FirstOrDefault(claim => claim.Type == "skogId").Value,
                name = claims.FirstOrDefault(claim => claim.Type == "name").Value,
                firstname = claims.FirstOrDefault(claim => claim.Type == "firstname").Value,
                surname = claims.FirstOrDefault(claim => claim.Type == "surname").Value,
                email = claims.FirstOrDefault(claim => claim.Type == "email").Value,
                image = claims.FirstOrDefault(claim => claim.Type == "image").Value,
                applicationRole = claims.FirstOrDefault(claim => claim.Type == "role").Value,
                accessToken = tokenResponse.accessToken,
                refreshToken = tokenResponse.refreshToken,
                identityToken = tokenResponse.identityToken,
                expiresIn = DateTime.Now.AddSeconds(tokenResponse.expiresIn).ToLocalTime()
            };

            return customClaims;
        }
    }
}
