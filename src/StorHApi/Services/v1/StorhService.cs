﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Infrastructure;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using StorHApi.Hubs;
using StorHApi.Models.AppSettings;
using StorHApi.Models.Enum;
using StorHApi.Models.View;
using StorHApi.Models.View.SignalR;
using StorHApi.Repo.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Services.v1
{
    public interface IStorhService
    {
        List<ItemDTO> GetItemsInGroup(int groupId, string email, bool isShoppingList);
        List<GroupDTO> GetGroups(string email);
        GroupDTO GetGroupById(string email, int id);
        List<GroupDTO> GetAllGroups();
        List<CategoryDTO> SearchCategory(string query);
        List<SubCategoryDTO> SearchSubCategory(string query);
        List<ItemManuDTO> SearchItemManu(string query);
        List<ItemDTO> SearchItem(string query);
        ItemDTO BarcodeSearch(string code);
        void UpdateItemCount(int groupId, int itemId, int count);
        void RegItem(RegItemDTO item);
        void CreateGroup(CreateGroupDTO data);
    }

    public class StorhService : IStorhService
    {
        private readonly IStorhRepo _storhRepo;
        private readonly IHubContext<StorHub, IStorhub> _hub;
        private readonly ISignalRepo _signalRepo;
        private readonly IOptions<AppSettings> _appSettings;

        public StorhService(IStorhRepo storhRepo, IConnectionManager signalRManager, ISignalRepo signalRepo, IOptions<AppSettings> appSettings)
        {
            _storhRepo = storhRepo;
            _hub = signalRManager.GetHubContext<StorHub, IStorhub>();
            _signalRepo = signalRepo;
            _appSettings = appSettings;
        }

        private CloudBlobContainer _getAzureContainer()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_appSettings.Value.BlobStorageConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            return blobClient.GetContainerReference(_appSettings.Value.BlobStorageContainerName);
        }

        public List<ItemDTO> GetItemsInGroup(int groupId, string email, bool isShoppingList)
        {
            return _storhRepo.GetItemsInGroup(groupId, email, isShoppingList);
        }

        public List<GroupDTO> GetGroups(string email)
        {
            return _storhRepo.GetGroups(email);
        }

        public List<GroupDTO> GetAllGroups()
        {
            return _storhRepo.GetAllGroups();
        }

        public GroupDTO GetGroupById(string email, int id)
        {
            return _storhRepo.GetGroupById(email, id);
        }

        public ItemDTO BarcodeSearch(string code)
        {
            return _storhRepo.BarcodeSearch(code);
        }

        public List<CategoryDTO> SearchCategory(string query)
        {
            return _storhRepo.SearchCategories(query);
        }

        public List<SubCategoryDTO> SearchSubCategory(string query)
        {
            return _storhRepo.SearchSubCategories(query);
        }

        public List<ItemManuDTO> SearchItemManu(string query)
        {
            return _storhRepo.SearchItemManu(query);
        }

        public List<ItemDTO> SearchItem(string query)
        {
            return _storhRepo.SearchItem(query);
        }

        public void CreateGroup(CreateGroupDTO data)
        {
            data.emailAdmin = data.emailAdmin.ToLower();
            data.invites = data.invites.Select(x => x.ToLower()).ToList();

            _storhRepo.CreateGroup(data);
            var receivers = _signalRepo.GetClients(data.invites);

            foreach (var i in receivers)
            {
                if (i.email.ToLower().Equals(data.emailAdmin))
                    continue;

                if (string.IsNullOrEmpty(i.connectionId))
                {
                   // user is not active --> send push
                }
                else
                {
                    _hub.SendToDevice(i.connectionId, new SignalrDTO()
                    {
                        channel = "",
                        identifier = SignalrIdentifier.groupInvite,
                        result = new GroupInviteDTO()
                        {
                            from = data.nameAdmin,
                            groupName = data.groupName
                        }
                    });
                }
            }
        }

        public void UpdateItemCount(int groupId, int itemId, int count)
        {
            _storhRepo.UpdateItemCount(groupId, itemId, count);
            _hub.SendToGroup(groupId, new SignalrDTO() {
                channel = groupId.ToString(),
                identifier = SignalrIdentifier.itemCountChanged,
                result = new ItemCountChangedDTO()
                {
                    itemId = itemId,
                    count = count
                } });
        }

        public void RegItem(RegItemDTO item)
        {
            _storhRepo.RegItem(item);
        }

    }
}
