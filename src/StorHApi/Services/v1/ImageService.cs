﻿using ImageSharp;
using ImageSharp.Formats;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using StorHApi.Models.AppSettings;
using StorHApi.Repo.v1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Services.v1
{
    public interface IImageService
    {
        Task storeImage(IFormFile file, int itemId);
    }

    public class ImageService : IImageService
    {
        private readonly IImageRepo _imageRepo;
        private readonly IOptions<AppSettings> _appSettings;

        public ImageService(IImageRepo imageRepo, IOptions<AppSettings> appSettings)
        {
            _imageRepo = imageRepo;
            _appSettings = appSettings;
        }

        private CloudBlobContainer _getAzureContainer()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_appSettings.Value.BlobStorageConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            return blobClient.GetContainerReference(_appSettings.Value.BlobStorageContainerName);
        }

        public async Task storeImage(IFormFile file, int itemId)
        {
            var fileName = $"{itemId}.jpg";
            var container = _getAzureContainer();
            var blob = container.GetBlockBlobReference(fileName);

            using (var stream = file.OpenReadStream())
                await blob.UploadFromStreamAsync(stream);
            
            _imageRepo.StoreImage(itemId, fileName);

        }
    }
    }
