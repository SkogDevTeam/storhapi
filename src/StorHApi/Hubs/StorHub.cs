﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Hubs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using StorHApi.Helpers;
using StorHApi.Models.Enum;
using StorHApi.Models.View.Auth;
using StorHApi.Models.View.SignalR;
using StorHApi.Repo.v1;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Hubs
{
    public interface IStorhub
    {
        void SendToGroup(int groupId, SignalrDTO signalRMessage);
        void SendToDevice(SignalrDTO signalRMessage);
        void itemCountChanged(SignalrDTO signalRMessage);
        void itemAdded(SignalrDTO signalRMessage);
        void itemRemoved(SignalrDTO signalRMessage);
        void groupInvite(SignalrDTO signalRMessage);
        void adminMessage(AdminResult result);
    }

    [HubName("storhub")]
    public class StorHub : Hub<IStorhub>
    {
        private IStorhRepo _storhRepo;
        private ISignalRepo _signalRepo;
        private readonly ILogger _logger;
        public StorHub(IStorhRepo storhRepo, ISignalRepo signalRepo, ILogger<StorHub> logger)
        {
            _storhRepo = storhRepo;
            _signalRepo = signalRepo;
            _logger = logger;

        }

        //private async Task OnStartup()
        //{
        //    var groups = _storhRepo.GetAllGroups();

        //    foreach (var group in groups)
        //        await Groups.Add(group.groupName);
        //}

        /// <summary>
        /// Send message to all admins
        /// </summary>
        /// <param name="chatMessage"></param>
        private void SendMessageToAdminFeed(AdminResult result)
        {
            Clients.Group("skogdevadmin").adminMessage(result);
        }

        public override Task OnConnected()
        {
            // var token = Context.QueryString.FirstOrDefault(x => x.Key == "access_token").Value;
            var token = Context.QueryString["access_token"];
            _logger.LogInformation("User connecting with: " + token);
            if (string.IsNullOrEmpty(token))
                throw new Exception("No token provided");

            var claims = ClaimsHelper.DecodeToken(token);
            _logger.LogInformation("User claims " + JObject.FromObject(claims).ToString());

            AddToGroup(claims, Context.ConnectionId);

            SendMessageToAdminFeed(new AdminResult()
            {
                status = 0,
                type = "message",
                result = "User connected: " + claims.email + " with client " + claims.client
            });

            SendMessageToAdminFeed(new AdminResult()
            {
                status = 0,
                type = "usercount",
                result = _signalRepo.GetUserCount()
            });
            return base.OnConnected();
        }

        public void AddToGroup(ClaimsDTO claims, string connectionId)
        {
            _signalRepo.AddClient(connectionId, claims.email, claims.client, claims.name);
            var group = _storhRepo.GetGroups(claims.email);
            _logger.LogInformation("Found group for " + claims.email + ": " + JArray.FromObject(group).ToString());
            if (group.Any())
            {
                _signalRepo.AddClientToGroup(Context.ConnectionId, group[0].groupName);
                _logger.LogInformation("..adding to group " + group[0].id.ToString());
                Groups.Add(Context.ConnectionId, group[0].id.ToString());
            }
                
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _signalRepo.RemoveClient(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }
    }

    public static class StorHubExtensions
    {
        public static void SendToGroup(this IHubContext<StorHub, IStorhub> hub, int groupId, SignalrDTO signalRMessage)
        {
            var groupIdentifier = groupId.ToString();
            switch (signalRMessage.identifier)
            {
                case SignalrIdentifier.itemCountChanged:
                    hub.Clients.Group(groupIdentifier).itemCountChanged(signalRMessage);
                    break;

                case SignalrIdentifier.itemAdded:
                    hub.Clients.Group(groupIdentifier).itemAdded(signalRMessage);
                    break;

                case SignalrIdentifier.itemRemoved:
                    hub.Clients.Group(groupIdentifier).itemRemoved(signalRMessage);
                    break;

            }
        }

        public static void SendToDevice(this IHubContext<StorHub, IStorhub> hub, string connectionId, SignalrDTO signalRMessage)
        {
            switch (signalRMessage.identifier)
            {
                case SignalrIdentifier.groupInvite:
                    hub.Clients.Client(connectionId).groupInvite(signalRMessage);
                    break;
            }
        }
    }
}
