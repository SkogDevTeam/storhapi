﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Hangfire;
using Hangfire.PostgreSql;
using StorHApi.Helpers;
using System.Collections;
using StorHApi.Models.AppSettings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.IO;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Serilog.Events;
using Serilog.Formatting.Json;
using StorHApi.Repo.v1;

namespace StorHApi
{
    public class Startup
    {
        private IConfigurationRoot _configuration { get; }
        private IHostingEnvironment _env;
        public Startup(IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            _env = env;
            ConfigureSeriLog();
            loggerFactory.AddSerilog();


            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName.ToLower()}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            _configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            // Add framework services.
            services.AddApplicationInsightsTelemetry(_configuration);


            services.AddCors(o => o.AddPolicy("allcors", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));

            // Add framework services.
            services.AddSignalR(options =>
            {
                
                options.Hubs.EnableDetailedErrors = true;
            });

            // App settings
            services.Configure<AppSettings>(s =>
            {
                s.StorHConnectionString = _configuration.GetSection("AppSettings:StorHConnectionString").Value;
                s.BlobStorageConnectionString = _configuration.GetSection("AppSettings:BlobStorageConnectionString").Value;
                s.BlobStorageContainerName = _configuration.GetSection("AppSettings:BlobStorageContainerName").Value;
                s.SkogDevFileStorageUrl = _configuration.GetSection("AppSettings:SkogDevFileStorageUrl").Value;
                s.SkogDevPushApiUrl = _configuration.GetSection("AppSettings:SkogDevPushApiUrl").Value;
                s.SkogIdTokenEndpoint = _configuration.GetSection("AppSettings:SkogIdTokenEndpoint").Value;
                s.SkogIdUserInfoEndpoint = _configuration.GetSection("AppSettings:SkogIdUserInfoEndpoint").Value;
                s.SignalRConnectionString = _configuration.GetSection("AppSettings:SignalRConnectionString").Value;
            });

            services.AddMvc();

            services.AddHangfire(options => options
               .UseStorage(new PostgreSqlStorage("Server=10.0.0.12;Port=5432; Database=storhangfiredev;User Id=developer;Password=developer"))
               .UseColouredConsoleLogProvider());
            
            services.AddSingleton<Helpers.ITokenHelper, Helpers.TokenHelper>();
            services.AddScoped<Services.v1.IStorhService, Services.v1.StorhService>();
            services.AddScoped<Services.v1.IImageService, Services.v1.ImageService>();
            services.AddScoped<Repo.v1.IStorhRepo, Repo.v1.StorhRepo>();
            services.AddScoped<Repo.v1.ISignalRepo, Repo.v1.SignalRepo>();
            services.AddScoped<Repo.v1.IImageRepo, Repo.v1.ImageRepo>();

            services.AddTransient<ClaimsPrincipal>(s => s.GetService<IHttpContextAccessor>().HttpContext.User);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, ISignalRepo signalRepo)
        {
            signalRepo.RemoveAllClients();

            loggerFactory.AddConsole(_configuration.GetSection("Logging"))
            .AddDebug()
            .AddSerilog();

            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = _configuration.GetSection("AppSettings:Authority").Value,
                AllowedScopes = new List<string>() { "storhapi" }, 
                InboundJwtClaimTypeMap = new Dictionary<string, string>(),
                AuthenticationScheme = "Bearer"
            });

           
            app.UseWebSockets();
            //app.UseSignalR("/signalr");
            app.Map("/signalr", map =>
            {
                map.UseCors(((config =>
                      config.AllowCredentials()
                              .AllowAnyHeader()
                              .AllowAnyMethod()
                              .AllowAnyOrigin())));

                map.RunSignalR();
            });

            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            });

            app.UseMvc();
            app.UseHangfireServer();
            app.UseHangfireDashboard("/fire", new DashboardOptions()
            {
                Authorization = new[] { new NoAuthFilter() },
                StatsPollingInterval = 60000
            });
        }
        protected virtual void ConfigureSeriLog()
        {
            var logFilePath = @"C:\ApplicationLogs\BussApi\";
            var logFileSize = 838860800;
            var logRetainCount = 10;

            Log.Logger = new LoggerConfiguration()

                .WriteTo.Logger(lc => lc
                    .Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Error)
                    .WriteTo.RollingFile(
                        formatter: new JsonFormatter(),
                        pathFormat: Path.Combine(logFilePath, _env.IsDevelopment() ? "StorhApiDev-error-{Date}.log" : "StorhApi-error-{Date}.log"),
                        fileSizeLimitBytes: logFileSize,
                        retainedFileCountLimit: logRetainCount
                    ))

                .WriteTo.Logger(lc => lc
                    .Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Information)
                    .Filter.ByExcluding(evt => !evt.MessageTemplate.Text.Contains("customlog"))
                    .WriteTo.RollingFile(
                        formatter: new JsonFormatter(),
                        pathFormat: Path.Combine(logFilePath, _env.IsDevelopment() ? "StorhApiDev-custom-info-{Date}.log" : "StorhApi-custom-info-{Date}.log"),
                        fileSizeLimitBytes: logFileSize,
                        retainedFileCountLimit: logRetainCount
                    ))

                .Enrich.WithProperty("Origin", "StorHApi Serilog")
                .Enrich.WithThreadId()
                .Enrich.WithMachineName()
                .Enrich.FromLogContext()

                .CreateLogger();
        }
    }
}
