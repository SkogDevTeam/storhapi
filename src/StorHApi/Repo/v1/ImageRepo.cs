﻿using Microsoft.Extensions.Options;
using Npgsql;
using StorHApi.Models.AppSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;

namespace StorHApi.Repo.v1
{
    public interface IImageRepo
    {
        void StoreImage(int itemId, string fileName);
    }

    public class ImageRepo : IImageRepo
    {
        private string connectionString;
        internal IDbConnection con { get { return new NpgsqlConnection(connectionString); } }

        public ImageRepo(IOptions<AppSettings> appSettings)
        {
            connectionString = appSettings.Value.StorHConnectionString;
        }

        private Dictionary<string, object> _generateParams(params object[] obs)
        {
            var dict = new Dictionary<string, object>();
            for (var i = 0; i < obs.Length; i++)
                dict["p" + i] = obs[i];
            return dict;
        }

        public void StoreImage(int itemId, string fileName)
        {
            var sql = @"Update item 
                        set image = @p1
                        WHERE id = @p0";

            using (var db = con)
            {
                db.Open();
                using (var trans = db.BeginTransaction())
                {
                    try
                    {
                        db.Execute(sql, _generateParams(itemId, fileName));
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
        }
    }
}
