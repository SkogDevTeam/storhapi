﻿using Microsoft.Extensions.Options;
using Npgsql;
using NpgsqlTypes;
using StorHApi.Models.AppSettings;
using StorHApi.Models.View;
using System;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using System.Data;
using StorHApi.Helpers;
using StorHApi.Models.Enum;

namespace StorHApi.Repo.v1
{

    public interface IStorhRepo
    {
        List<ItemDTO> GetItemsInGroup(int groupId, string userEmail, bool isShoppingList);
        List<GroupDTO> GetGroups(string email);
        GroupDTO GetGroupById(string email, int id);
        List<GroupDTO> GetAllGroups();
        ItemDTO BarcodeSearch(string code);
        List<CategoryDTO> SearchCategories(string query);
        List<SubCategoryDTO> SearchSubCategories(string query);
        List<ItemManuDTO> SearchItemManu(string query);
        List<ItemDTO> SearchItem(string query);
        void UpdateItemCount(int groupId, int itemId, int count);
        void RegItem(RegItemDTO item);
        void CreateGroup(CreateGroupDTO data);
    }

    public class StorhRepo : IStorhRepo
    {
        private string connectionString;
        internal IDbConnection con { get { return new NpgsqlConnection(connectionString); } }

        public StorhRepo(IOptions<AppSettings> appSettings)
        {
            connectionString = appSettings.Value.StorHConnectionString;
        }

        private Dictionary<string, object> _generateParams(params object[] obs)
        {
            var dict = new Dictionary<string, object>();
            for (var i = 0; i < obs.Length; i++)
                dict["p" + i] = obs[i];
            return dict;
        }

        /// <summary>
        /// Get all items in group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public List<ItemDTO> GetItemsInGroup(int groupId, string userEmail, bool isShoppingList)
        {
            var sql = @"WITH allowed_access AS (SELECT COUNT(id) 
		                                        FROM user_in_group 
		                                        WHERE id_group = @p0
		                                        AND email ILIKE @p1
		                                        AND accepted = TRUE)
                        SELECT i.id,
                               i.item_name, 
                               i.item_descr,
                               i.image,
                               ic.id as item_cat_id,
                               ic.item_cat_name,
                               isc.id as item_subcat_id,  
                               isc.item_subcat_name, 
                               im.manu_name, 
                               iig.item_count,
                               iig.is_shopping_list
                        FROM item i
                        LEFT JOIN item_category ic ON i.item_cat_id = ic.id
                        LEFT JOIN item_subcategory isc ON i.item_subcat_id = isc.id
                        LEFT JOIN item_manu im ON i.item_manu_id = im.id
                        LEFT JOIN item_in_group iig ON i.id = iig.id_item
                        WHERE iig.id_group = @p0
                        AND iig.is_shopping_list = @p2
                        AND (SELECT * FROM allowed_access) > 0
                        GROUP BY i.id, 
	                         i.item_name, 
	                         i.item_descr,
                             i.image,
	                         ic.id, 
	                         ic.item_cat_name, 
	                         isc.id, 
	                         isc.item_subcat_name, 
	                         im.manu_name, 
	                         iig.item_count,
                             iig.is_shopping_list";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(groupId, userEmail, isShoppingList))
                            .Select(x => new ItemDTO()
                            {
                                id = x.id,
                                itemName = x.item_name,
                                itemDesc = x.item_descr,
                                itemCat = x.item_cat_id ?? -1,
                                itemCatName = x.item_cat_name,
                                itemSubCat = x.item_subcat_id ?? -1,
                                itemSubCatName = x.item_subcat_name,
                                itemManu = x.manu_name,
                                image = x.image,
                                count = x.item_count,
                                groupId = groupId,
                                isShoppingList = x.is_shopping_list
                            })
                            .ToList();
            }
        }

        /// <summary>
        /// Add or Update item in group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="itemId"></param>
        /// <param name="count"></param>
        public void UpdateItemCount(int groupId, int itemId, int count)
        {
            var sql = @"DELETE FROM item_in_group iig
                        WHERE iig.id_group = @p0
                        AND iig.id_item = @p1";

            if (count > 0)
            {
                sql = @"INSERT INTO item_in_group (id_group, id_item, item_count)
                        VALUES (@p0, @p1, @p2)
                        ON CONFLICT (id_group, id_item)
                        DO UPDATE SET item_count = @p2
                        WHERE item_in_group.id_group = @p0
                        AND item_in_group.id_item = @p1";
            }

            using (var db = con)
            {
                db.Open();
                using (var trans = db.BeginTransaction())
                {
                    try
                    {
                        db.Execute(sql, _generateParams(groupId, itemId, count));
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public ItemDTO BarcodeSearch(string code)
        {
            var sql = @"SELECT i.id,
                               i.item_name, 
                               i.item_descr,
                               i.item_code, 
                               ic.id as item_cat_id,
                               ic.item_cat_name,
                               isc.id as item_subcat_id,  
                               isc.item_subcat_name, 
                               im.manu_name
                        FROM item i
                        LEFT JOIN item_category ic ON i.item_cat_id = ic.id
                        LEFT JOIN item_subcategory isc ON i.item_subcat_id = isc.id
                        LEFT JOIN item_manu im ON i.item_manu_id = im.id
                        WHERE i.item_code = @p0
                        GROUP BY i.id, i.item_name, i.item_descr, i.item_code, ic.id, ic.item_cat_name, isc.id, isc.item_subcat_name, im.manu_name";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(code))
                            .Select(x => new ItemDTO()
                            {
                                id = x.id,
                                itemName = x.item_name,
                                itemDesc = x.item_descr,
                                itemCat = x.item_cat_id,
                                itemCatName = x.item_cat_name,
                                itemSubCat = x.item_subcat_id ?? -1,
                                itemSubCatName = x.item_subcat_name,
                                itemManu = x.manu_name,
                                image = x.image
                            })
                            .FirstOrDefault();
            }
        }

        public List<GroupDTO> GetGroups(string email)
        {
            var sql = @"SELECT g.* 
                        FROM ""group"" g
                        LEFT JOIN user_in_group uig ON g.id = uig.id_group
                        WHERE uig.email ILIKE @p0
                        AND uig.accepted = true";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(email))
                            .Select(x => new GroupDTO()
                            {
                                id = x.id,
                                groupName = x.group_name,
                                groupCreated = x.group_created,
                                emailAdmin = x.email_admin,
                                nameAdmin = x.name_admin
                            })
                            .ToList();
            }
        }

        public List<GroupDTO> GetAllGroups()
        {
            var sql = @"SELECT g.* 
                        FROM ""group"" g
                        LEFT JOIN user_in_group uig ON g.id = uig.id_group
                        WHERE uig.accepted = true";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql)
                            .Select(x => new GroupDTO()
                            {
                                id = x.id,
                                groupName = x.group_name,
                                groupCreated = x.group_created,
                                emailAdmin = x.email_admin,
                                nameAdmin = x.name_admin
                            })
                            .ToList();
            }
        }

        public GroupDTO GetGroupById(string email, int id)
        {
            var sql = @"SELECT g.* 
                        FROM ""group"" g
                        LEFT JOIN user_in_group uig ON g.id = uig.id_group
                        WHERE uig.email ILIKE @p0
                        AND g.id = @p1
                        AND uig.accepted = true";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(email, id))
                            .Select(x => new GroupDTO()
                            {
                                id = x.id,
                                groupName = x.group_name,
                                groupCreated = x.group_created,
                                emailAdmin = x.email_admin,
                                nameAdmin = x.name_admin
                            })
                            .FirstOrDefault();
            }
        }

        public List<CategoryDTO> SearchCategories(string query)
        {
            var sql = @"SELECT * 
                        FROM item_category ic
                        WHERE ic.item_cat_name ILIKE '%' || @p0 || '%'";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(query))
                            .Select(x => new CategoryDTO()
                            {
                                id = x.id,
                                catName = x.item_cat_name
                            })
                            .OrderBy(r => r.catName)
                            .ToList();
            }
        }

        public List<SubCategoryDTO> SearchSubCategories(string query)
        {
            var sql = @"SELECT * 
                        FROM item_subcategory isc
                        WHERE isc.item_subcat_name ILIKE '%' || @p0 || '%'";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(query))
                            .Select(x => new SubCategoryDTO()
                            {
                                id = x.id,
                                subCatName = x.item_subcat_name
                            })
                            .OrderBy(r => r.subCatName)
                            .ToList();
            }
        }

        public List<ItemManuDTO> SearchItemManu(string query)
        {
            var sql = @"SELECT * 
                        FROM item_manu im
                        WHERE im.manu_name ILIKE '%' || @p0 || '%'";

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(query))
                            .Select(x => new ItemManuDTO()
                            {
                                id = x.id,
                                manuName = x.manu_name
                            })
                            .OrderBy(r => r.manuName)
                            .Take(6)
                            .ToList();
                            
                           
            }
        }

        public List<ItemDTO> SearchItem(string query)
        {
            var sql = @"SELECT i.id,
                               i.item_name, 
                               i.item_descr,
                               i.image,
                               ic.id as item_cat_id,
                               ic.item_cat_name,
                               isc.id as item_subcat_id,  
                               isc.item_subcat_name, 
                               im.manu_name
                        FROM item i
                        LEFT JOIN item_category ic ON i.item_cat_id = ic.id
                        LEFT JOIN item_subcategory isc ON i.item_subcat_id = isc.id
                        LEFT JOIN item_manu im ON i.item_manu_id = im.id
                        WHERE i.item_name ILIKE '%' || @p0 || '%'
                        OR ic.item_cat_name ILIKE '%' || @p0 || '%'
                        OR im.manu_name ILIKE '%' || @p0 || '%'
                        GROUP BY i.id, i.item_name, i.item_descr, i.image, ic.id, ic.item_cat_name, isc.id, isc.item_subcat_name, im.manu_name";


            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql,
                            _generateParams(query))
                            .Select(x => new ItemDTO()
                            {
                                id = x.id,
                                itemName = x.item_name,
                                itemDesc = x.item_descr,
                                itemCat = x.item_cat_id ?? -1,
                                itemCatName = x.item_cat_name,
                                itemSubCat = x.item_subcat_id ?? -1,
                                itemSubCatName = x.item_subcat_name ?? "",
                                itemManu = x.manu_name,
                                image = x.image
                            })
                            .Take(7)
                            .ToList();
            }
        }

        public void RegItem(RegItemDTO item)
        {
            var sql = @"INSERT INTO item (item_name, 
                                          item_descr, 
                                          item_cat_id, 
                                          item_manu_id,
                                          item_code,
                                          item_code_format,
                                          image) 
                        VALUES (@p0, @p1, @p2, @p3, @p4, @p5, @p6)";

            // Registert manufacturer if new
            if (item.itemManu.id == -1)
                item.itemManu.id = RegManu(item.itemManu.manuName);

            using (var db = con)
            {
                db.Open();
                using (var trans = db.BeginTransaction())
                {
                    try
                    {
                        db.Execute(sql, _generateParams(item.itemName,
                                               item.itemDescr,
                                               item.itemCat.id,
                                               item.itemManu.id,
                                               item.itemCode,
                                               item.itemCodeFormat,
                                               item.image ?? ""));
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Register an item manufacturer
        /// </summary>
        /// <param name="name"></param>
        public int RegManu(string name)
        {
            var sql = @"INSERT INTO item_manu (manu_name) 
                        VALUES (@p0) 
                        RETURNING id";
            using (var db = con)
            {
                db.Open();
                using (var trans = db.BeginTransaction())
                {
                    try
                    {
                        var id = db.ExecuteScalar<int>(sql, _generateParams(name));
                        trans.Commit();
                        return id;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Register a new group, and link creator and invites to that group
        /// </summary>
        /// <param name="name"></param>
        public void CreateGroup(CreateGroupDTO data)
        {
            var sql = @"INSERT INTO ""group"" (group_name, group_created, email_admin, name_admin) 
                        VALUES (@p0, @p1, @p2, @p3)
                        RETURNING id";

            using (var db = con)
            {
                db.Open();
                using (var trans = db.BeginTransaction())
                {
                    try
                    {
                        var id = db.ExecuteScalar<int>(sql, _generateParams(data.groupName, data.groupCreated, data.emailAdmin, data.nameAdmin));
                        
                        var userInGroupSql = @"INSERT INTO user_in_group (email, id_group, accepted)
                                                VALUES (@p0, @p1, @p2)";

                        var userInGroupParams = new Dictionary<string, object>();
                        userInGroupParams["p0"] = data.emailAdmin;
                        userInGroupParams["p1"] = id;
                        userInGroupParams["p2"] = true;

                        for (var i = 0; i < data.invites.Count; i+=3)
                        {
                            userInGroupSql += ", (@p" + (i + 3) + ",@p" + (i + 4) + ",@p" + (i + 5) + ")";
                            userInGroupParams["p" + (i + 3)] = data.invites[i];
                            userInGroupParams["p" + (i + 4)] = id;
                            userInGroupParams["p" + (i + 5)] = false;
                        }
                        userInGroupSql += " ON CONFLICT DO NOTHING";

                        db.Execute(userInGroupSql, userInGroupParams);
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
        }




    }
}
