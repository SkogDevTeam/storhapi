﻿using Microsoft.Extensions.Options;
using Npgsql;
using StorHApi.Models.AppSettings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Threading.Tasks;
using StorHApi.Models.View;

namespace StorHApi.Repo.v1
{
    public interface ISignalRepo
    {
        void AddClient(string connectionId, string email, string client, string fullName);
        void RemoveClient(string connectionId);
        void AddClientToGroup(string connectionId, string groupName);
        void RemoveClientFromGroup(string connectionId, string groupName);
        void RemoveClientFromAllGroups(string connectionId);
        void RemoveAllClients();
        //List<ClientDTO> GetClientsInGroup(string groupName);
        List<string> GetClientGroups(string connection_id);
        List<SignalRClient> GetClients(List<string> emails);
        int GetUserCount();
    }

    public class SignalRepo : ISignalRepo
    {
        private string connectionString;
        internal IDbConnection con { get { return new NpgsqlConnection(connectionString); } }

        public SignalRepo(IOptions<AppSettings> appSettings)
        {
            connectionString = appSettings.Value.SignalRConnectionString;
        }

        private Dictionary<string, object> _generateParams(params object[] obs)
        {
            var dict = new Dictionary<string, object>();
            for (var i = 0; i < obs.Length; i++)
                dict["p" + i] = obs[i];
            return dict;
        }

        public void AddClient(string connectionId, string email, string client, string fullName)
        {
            var sql = @"INSERT INTO client (connection_id, connected, email, client, full_name)
                        VALUES (@p0, @p1, @p2, @p3, @p4)";

            using (var db = con)
            {
                db.Open();
                db.Execute(sql, _generateParams(connectionId, DateTime.Now, email, client, fullName));
            }

        }

        public void RemoveClient(string connectionId)
        {
            RemoveClientFromAllGroups(connectionId);

            var sql = @"DELETE FROM client
                        WHERE connection_id = @p0";
            using (var db = con)
            {
                db.Open();
                db.Execute(sql, _generateParams(connectionId));
            }
        }

        public void AddClientToGroup(string connectionId, string groupName)
        {
            var sql = @"INSERT INTO client_in_group (connection_id, group_name, connected)
                        VALUES (@p0, @p1, @p2)";
            using (var db = con)
            {
                db.Open();
                db.Execute(sql, _generateParams(connectionId, groupName, DateTime.Now));
            }
        }

        public void RemoveClientFromGroup(string connectionId, string groupName)
        {
            var sql = @"DELETE FROM client_in_group
                        WHERE connection_id = @p0
                        AND group_name = @p1";
            using (var db = con)
            {
                db.Open();
                db.Execute(sql, _generateParams(connectionId, groupName, DateTime.Now));
            }
        }

        public void RemoveClientFromAllGroups(string connectionId)
        {
            var sql = @"DELETE FROM client_in_group
                        WHERE connection_id = @p0";
            using (var db = con)
            {
                db.Open();
                db.Execute(sql, _generateParams(connectionId));
            }
        }

        public void RemoveAllClients()
        {
            var sql = @"DELETE FROM client";
            var sql2 = @"DELETE FROM client_in_group";
            using (var db = con)
            {
                db.Open();
                db.Execute(sql);
                db.Execute(sql2);
            }
        }

        //public List<ClientDTO> GetClientsInGroup(string groupName)
        //{
        //    var sql = @"SELECT *
        //                FROM client c
        //                JOIN client_in_group cig 
        //                ON c.connection_id = cig.connection_id
        //                AND cig.group_name = @p0";

        //    using (var db = con)
        //    {
        //        db.Open();
        //        var dbclients = db.Query<Client>(sql, _generateParams(groupName)).ToList();

        //    }
        //}

        public List<SignalRClient> GetClients(List<string> emails)
        {
            var emailArray = "('" + string.Join("','", emails) + "')";

            var sql = @"SELECT c.email, c.connection_id
                        FROM client c
                        WHERE c.email IN " + emailArray;

            using (var db = con)
            {
                db.Open();
                return db.Query<dynamic>(sql)
                    .Select((res) => new SignalRClient()
                    {
                        email = res.email,
                        connectionId = res.connection_id
                    }).ToList();
            }
        }

        public List<string> GetClientGroups(string connection_id)
        {
            var sql = @"SELECT cig.group_name
                        FROM client_in_group cig
                        WHERE cig.connection_id = @p0";

            using (var db = con)
            {
                db.Open();
                return db.Query<string>(sql, _generateParams(connection_id)).ToList();
            }
        }


        public int GetUserCount()
        {
            var sql = @"SELECT COUNT(*)
                        FROM client u 
                        WHERE u.client ILIKE 'storh'";

            using (var db = con)
            {
                db.Open();
                return db.Query<int>(sql).FirstOrDefault();
            }
        }
    }
}
