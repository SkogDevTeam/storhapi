﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.AppSettings
{
    public class AppSettings
    {
        public string StorHConnectionString { get; set; }
        public string BlobStorageConnectionString { get; set; }
        public string BlobStorageContainerName { get; set; }
        public string SkogDevFileStorageUrl { get; set; }
        public string SkogDevPushApiUrl { get; set; }
        public string SkogIdTokenEndpoint { get; set; }
        public string SkogIdUserInfoEndpoint { get; set; }
        public string SignalRConnectionString { get; set; }
    }
}
