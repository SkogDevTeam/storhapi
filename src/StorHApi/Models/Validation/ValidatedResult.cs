﻿using StorHApi.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.Validation
{
    public class ValidatedResult
    {
        public bool valid { get; set; }
        public StorHApiResponse result { get; set; }
    }
}
