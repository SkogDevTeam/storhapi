﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class SignalRClient
    {
        public string email { get; set; }
        public string connectionId { get; set; }
    }
}
