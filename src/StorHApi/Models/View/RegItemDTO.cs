﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class RegItemDTO
    {
        public string itemCode { get; set; }
        public string itemCodeFormat { get; set; }
        public CategoryDTO itemCat { get; set; }
        public ItemManuDTO itemManu { get; set; }
        public string itemName { get; set; }
        public string itemDescr { get; set; }
        public string image { get; set; }
    }
}
