﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class ItemCountChangedDTO
    {
        public int itemId { get; set; }
        public int count { get; set; }
    }
}
