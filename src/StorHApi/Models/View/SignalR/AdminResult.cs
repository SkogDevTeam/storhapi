﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View.SignalR
{
    public class AdminResult
    {
        public int status { get; set; }
        public string type { get; set; }
        public object result { get; set; }
    }
}
