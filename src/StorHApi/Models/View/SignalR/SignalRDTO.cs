﻿using StorHApi.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View.SignalR
{
    public class SignalrDTO
    {
        public string channel { get; set; }
        public SignalrIdentifier identifier { get; set; }
        public dynamic result { get; set; }
    }
}
