﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class ItemManuDTO
    {
        public int id { get; set; }
        public string manuName { get; set; }
    }
}
