﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class GroupInviteDTO
    {
        public string from { get; set; }
        public string groupName { get; set; }
        public List<string> connectionIds { get; set; }
    }
}
