﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace StorHApi.Models.View.Auth
{
    public class TokenResponseDTO
    {
        public string accessToken { get; set; }
        public string error { get; set; }
        public long expiresIn { get; set; }
        public string httpErrorReason { get; set; }
        public HttpStatusCode httpErrorStatusCode { get; set; }
        public string identityToken { get; set; }
        public bool isError { get; set; }
        public bool isHttpError { get; set; }
        public JObject Json { get; set; }
        public string raw { get; set; }
        public string refreshToken { get; set; }
        public string tokenType { get; set; }
    }
}
