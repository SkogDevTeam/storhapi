﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View.Auth
{
    public class ExchangeTokenDTO
    {
        public string code { get; set; }
        public string redirectUrl { get; set; }
        public string appVersion { get; set; }
        public string device { get; set; }
        public string platform { get; set; }
        public string osVersion { get; set; }
        public string client { get; set; }
        public string secret { get; set; }
    }
}
