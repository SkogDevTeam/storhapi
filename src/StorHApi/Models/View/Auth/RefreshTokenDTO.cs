﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View.Auth
{
    public class RefreshTokenDTO
    {
        public string email { get; set; }
        public string device { get; set; }
        public string platform { get; set; }
        public string token { get; set; }
        public string client { get; set; }
        public string secret { get; set; }
    }
}
