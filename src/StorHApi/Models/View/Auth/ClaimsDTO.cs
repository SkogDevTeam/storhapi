﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View.Auth
{
    public class ClaimsDTO
    {
        public string skogId { get; set; }

        public string firstname { get; set; }

        public string surname { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public string image { get; set; }

        public string accessToken { get; set; }

        public string refreshToken { get; set; }

        public string identityToken { get; set; }

        public string applicationRole { get; set; }

        public DateTime expiresIn { get; set; }
        public string client { get; set; }
    }
}
