﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class PushDeviceDTO
    {
        public string id { get; set; }
        public string regId { get; set; }
        public string platform { get; set; }
        public string uuid { get; set; }
        public string model { get; set; }
        public string manufacturer { get; set; }
        public string application { get; set; }
        public string appversion { get; set; }
        public int skogId { get; set; }
        public string firstname { get; set; }
        public string surname { get; set; }
        public string role { get; set; }
        public string email { get; set; }
    }
}
