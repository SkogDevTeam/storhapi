﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class SubCategoryDTO
    {
        public int id { get; set; }
        public string subCatName { get; set; }
    }
}
