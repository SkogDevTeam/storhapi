﻿using StorHApi.Models.Enum;

namespace StorHApi.Models.View
{
    public class ItemDTO
    {
        public int id { get; set; }
        public string itemName { get; set; }
        public string itemDesc { get; set; }
        public int itemCat { get; set; }
        public string itemCatName { get; set; }
        public int itemSubCat { get; set; }
        public string itemSubCatName { get; set; }
        public string itemManu { get; set; }
        public int count { get; set; }
        public int groupId { get; set; }
        public string code { get; set; }
        public string image { get; set; }
        public bool isShoppingList { get; set; }
    }
}
