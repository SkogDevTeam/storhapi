﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class GroupDTO
    {
        public int id { get; set; }
        public string groupName { get; set; }
        public DateTime groupCreated { get; set; }
        public string emailAdmin { get; set; }
        public string nameAdmin { get; set; }
    }
}
