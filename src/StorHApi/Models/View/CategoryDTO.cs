﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.View
{
    public class CategoryDTO
    {
        public int id { get; set; }
        public string catName { get; set; }
    }
}
