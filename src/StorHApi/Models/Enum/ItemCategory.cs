﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Models.Enum
{
    public enum ItemCategory
    {
        [Display(Name = "Melkeprodukter")]
        DairyProducts = 1,

        [Display(Name = "Brødvarer")]
        Breads = 2,

        [Display(Name = "Egg")]
        Egg = 3,

        [Display(Name = "Fugl og Kjøtt")]
        PoultryAndMeat = 4,

        [Display(Name = "Sjømat")]
        Seafoods = 5,

        [Display(Name = "Nøtter of frø")]
        NutsAndSeeds = 6,

        [Display(Name = "Belgfrukter")]
        Legumes = 7,

        [Display(Name = "Frukt og grønt")]
        FruitAndVegetables = 8,

        [Display(Name = "Søtsaker")]
        SugarAndSweets = 9,

        [Display(Name = "Smør og oljer")]
        ButterAndOils = 10,

        [Display(Name = "Drikkevarer")]
        Beverages = 11,

        [Display(Name = "Barnemat")]
        InfantFood = 12,

        [Display(Name = "Sopp")]
        Mushrooms = 13,

        [Display(Name = "Basisvarer")]
        StapleFoods = 14,

        [Display(Name = "Personlig hygiene")]
        PersonalCare = 15,

        [Display(Name = "Tobakk")]
        Tobacco = 16,

        [Display(Name = "Papir og hushold")]
        PaperGoods = 17,

        [Display(Name = "Rengjøring")]
        Cleaners = 18,

        [Display(Name = "Krydder")]
        Spices = 19,

        [Display(Name = "Annet")]
        Other = 20,
    }
}
