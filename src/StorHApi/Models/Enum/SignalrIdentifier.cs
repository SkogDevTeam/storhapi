﻿using System.ComponentModel.DataAnnotations;

namespace StorHApi.Models.Enum
{
    public enum SignalrIdentifier
    {
        [Display(Name = "itemCountChanged")]
        itemCountChanged = 1,

        [Display(Name = "itemAdded")]
        itemAdded = 2,

        [Display(Name = "itemRemoved")]
        itemRemoved = 3,

        [Display(Name = "groupInvite")]
        groupInvite = 4
    }
}
