﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace StorHApi.Models.Response
{
    public class StorHApiResponse
    {
        public StorHApiResponse()
        {
            statusCode = HttpStatusCode.OK;
            message = "success";
            result = new object();
        }

        public StorHApiResponse(object res)
        {
            statusCode = HttpStatusCode.OK;
            message = "success";
            result = res;
        }

        public StorHApiResponse(string msg)
        {
            statusCode = HttpStatusCode.InternalServerError;
            message = msg;
            result = msg;
        }

        public HttpStatusCode statusCode { get; set; }
        public string message { get; set; }
        public object result { get; set; }
    }
}
