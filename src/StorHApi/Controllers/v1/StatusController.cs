﻿using Microsoft.AspNetCore.Mvc;
using StorHApi.Models.Response;
using StorHApi.Services.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorHApi.Controllers.v1
{
    public class StatusController : Controller
    {
        private readonly IStorhService _storhService;
        public StatusController(IStorhService storhService)
        {
            _storhService = storhService;
        }

        [Route("/v1/status/")]
        [HttpGet]
        public IActionResult GetStatus()
        {
            try
            {

                var items = _storhService.SearchItem("Toro");
                return new OkObjectResult(new StorHApiResponse(items));

            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException));
            }
        }
    }
}

