﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using StorHApi.Helpers;
using StorHApi.Models.Response;
using StorHApi.Services.v1;
using System;
using System.Security.Claims;
using StorHApi.Models.View;

namespace StorHApi.Controllers.v1
{

    public class DataController : Controller
    {
        private readonly IStorhService _storhService;
        private readonly ClaimsPrincipal _principal;

        public DataController(IStorhService storhService, ClaimsPrincipal principal)
        {
            _storhService = storhService;
            _principal = principal;
        }

        [Route("v1/data/searchcat")]
        [Authorize]
        [HttpGet("{query}")]
        public IActionResult SearchCategories(string query)
        {
            if (query == null)
                query = "";
            try
            {
                var places = _storhService.SearchCategory(query);
                return new OkObjectResult(new StorHApiResponse(places));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/data/searchsubcat")]
        [Authorize]
        [HttpGet("{query}")]
        public IActionResult SearchSubCategories(string query)
        {
            if (query == null)
                query = "";
            try
            {
                var places = _storhService.SearchSubCategory(query);
                return new OkObjectResult(new StorHApiResponse(places));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/data/searchmanu")]
        [Authorize]
        [HttpGet("{query}")]
        public IActionResult SearchManu(string query)
        {
            if (query == null)
                query = "";
            try
            {
                var items = _storhService.SearchItemManu(query);
                return new OkObjectResult(new StorHApiResponse(items));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/data/searchitem")]
        [AllowAnonymous]
        [HttpGet("{query}")]
        public IActionResult SearchItem(string query)
        {
            if (query == null)
                query = "";
            try
            {
                var items = _storhService.SearchItem(query);
                return new OkObjectResult(new StorHApiResponse(items));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }
    }
}
