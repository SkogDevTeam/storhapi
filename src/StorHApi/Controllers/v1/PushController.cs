﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using StorHApi.Helpers;
using StorHApi.Models.AppSettings;
using StorHApi.Models.Response;
using StorHApi.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace StorHApi.Controllers.v1
{
    public class PushController : Controller
    {

        private IOptions<AppSettings> _appSettings;
        private ClaimsPrincipal _principal;
        public PushController(IOptions<AppSettings> appSettings, ClaimsPrincipal principal)
        {
            _appSettings = appSettings;
            _principal = principal;
        }

        [Authorize]
        [Route("/v1/push/regdevice")]
        [HttpPost("{device}")]
        public async Task<IActionResult> RegDevice([FromBody] PushDeviceDTO device)
        {
            var valResult = Validator.ValidatePushDevice(device);

            if (!valResult.valid)
                return new BadRequestObjectResult(valResult.result);

            using (var client = new HttpClient())
            {
                try
                {
                    var claims = ClaimsHelper.GetClaims(_principal);
                    device.role = claims.applicationRole;
                    device.email = claims.email;
                    var response = await client.PostAsync(_appSettings.Value.SkogDevPushApiUrl + "storhpush/regdevice", new StringContent(JObject.FromObject(device).ToString(), Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        return new OkObjectResult(new StorHApiResponse());
                    }
                    else
                    {

                        return new BadRequestObjectResult(new StorHApiResponse("Error connecting to PushApi: " + _appSettings.Value.SkogDevPushApiUrl));
                    }
                }
                catch (Exception ex)
                {
                    return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException));
                }
            }
        }
    }
}
