﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using StorHApi.Helpers;
using StorHApi.Models.Response;
using StorHApi.Services.v1;
using System;
using System.Security.Claims;
using StorHApi.Models.View;
using System.Collections.Generic;
using System.Linq;

namespace StorHApi.Controllers.v1
{
    public class GroupController : Controller
    {
        private readonly IStorhService _storhService;
        private readonly ClaimsPrincipal _principal;

        public GroupController(IStorhService storhService, ClaimsPrincipal principal)
        {
            _storhService = storhService;
            _principal = principal;
        }

        [Route("v1/group/getgroups")]
        [Authorize]
        [HttpGet]
        public IActionResult GetGroups()
        {
            try
            {
                var sub = _principal.Claims.Where(c => c.Type.Equals("sub")).Select(x => x.Value).FirstOrDefault();
                var groups = _storhService.GetGroups(sub);
                return new OkObjectResult(new StorHApiResponse(groups));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/group/getgroup")]
        [Authorize]
        [HttpGet]
        public IActionResult GetGroup(int id)
        {
            try
            {
                var sub = _principal.Claims.Where(c => c.Type.Equals("sub")).Select(x => x.Value).FirstOrDefault();
                var groups = _storhService.GetGroupById(sub, id);
                return new OkObjectResult(new StorHApiResponse(groups));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/group/create")]
        [Authorize]
        [HttpPost]
        public IActionResult CreateGroup([FromBody] CreateGroupDTO data)
        {
            try
            {
                var claims = ClaimsHelper.GetClaims(_principal);
                data.emailAdmin = claims.email;
                data.nameAdmin = claims.firstname;

                _storhService.CreateGroup(data);
                return new OkObjectResult(new StorHApiResponse());
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

    }
}