﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using StorHApi.Helpers;
using StorHApi.Models.AppSettings;
using StorHApi.Models.Response;
using StorHApi.Models.View.Auth;
using System;
using System.Threading.Tasks;

namespace StorHApi.Controllers.v1
{
    public class AuthController : Controller
    {
        private ITokenHelper _tokenHelper;
        private IOptions<AppSettings> _appSettings;
        

        public AuthController(ITokenHelper tokenHelper, IOptions<AppSettings> appSettings)
        {
            _tokenHelper = tokenHelper;
            _appSettings = appSettings;
        }

        [Route("v1/auth/login")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginDTO dto)
        {
            try
            {
                var tokenResponse = await _tokenHelper.resourceOwnerAsync(_appSettings.Value.SkogIdTokenEndpoint, "storh", "thereisnospoon",dto.email, dto.password, "openid storhapi");

                if (tokenResponse.isError)
                {
                    return new BadRequestObjectResult(new StorHApiResponse("An error occurred during login: " + tokenResponse.error));
                }

                var claims = await _tokenHelper.getUserInfo(_appSettings.Value.SkogIdUserInfoEndpoint, tokenResponse, true);
                return new OkObjectResult(new StorHApiResponse(claims));

            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException));
            }

        }

        [Route("v1/auth/exchangetoken")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ExchangeToken([FromBody] ExchangeTokenDTO dto)
        {
            try
            {
                var tokenResponse = await _tokenHelper.requestAuthorizationCodeAsync(_appSettings.Value.SkogIdTokenEndpoint, dto.client, dto.secret, dto.code, dto.redirectUrl);

                if (tokenResponse.isError)
                {
                    return new BadRequestObjectResult(new StorHApiResponse("An error occured during authorizationcode-request"));
                }

                var claims = await _tokenHelper.getUserInfo(_appSettings.Value.SkogIdUserInfoEndpoint, tokenResponse, false);
                return new OkObjectResult(new StorHApiResponse(claims));

            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException));
            }

        }

        [Route("v1/auth/refreshtoken")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> RefreshTokenForClient([FromBody] RefreshTokenDTO tokenDTO)
        {
            var tokenResponse = await _tokenHelper.requestRefreshTokenAsync(_appSettings.Value.SkogIdTokenEndpoint, tokenDTO.client, tokenDTO.secret, tokenDTO.token);

            if (tokenResponse.isError)
            {
                return new BadRequestObjectResult(new StorHApiResponse("An error occured during refreshToken-request"));
            }

            var claims = await _tokenHelper.getUserInfo(_appSettings.Value.SkogIdUserInfoEndpoint, tokenResponse, true);

            return new OkObjectResult(new StorHApiResponse(claims));
        }

    }
}
