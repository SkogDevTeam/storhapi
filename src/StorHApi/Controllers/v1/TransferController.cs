﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StorHApi.Models.Response;
using StorHApi.Services.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StorHApi.Controllers.v1
{
    public class TransferController : Controller
    {
        private readonly IHttpContextAccessor _context;
        private readonly IStorhService _storhService;
        private readonly ClaimsPrincipal _principal;

        public TransferController(IHttpContextAccessor contextAccessor, IStorhService storhService, ClaimsPrincipal principal)
        {
            _storhService = storhService;
            _principal = principal;
            _context = contextAccessor;
        }

        [Route("v1/transfer/downloadimage")]
        [Authorize]
        [HttpGet]
        public IActionResult GetGroups()
        {
            try
            {
                var sub = _principal.Claims.Where(c => c.Type.Equals("sub")).Select(x => x.Value).FirstOrDefault();
                var groups = _storhService.GetGroups(sub);
                return new OkObjectResult(new StorHApiResponse(groups));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }
    }
}
