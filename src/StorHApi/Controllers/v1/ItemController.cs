﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using StorHApi.Helpers;
using StorHApi.Models.Response;
using StorHApi.Services.v1;
using System;
using System.Security.Claims;
using StorHApi.Models.View;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace StorHApi.Controllers.v1
{
    
    public class ItemController : Controller
    {
        private readonly IHttpContextAccessor _context;
        private readonly IStorhService _storhService;
        private readonly IImageService _imageService;
        private readonly ClaimsPrincipal _principal;

        public ItemController(IHttpContextAccessor context, 
                              IStorhService storhService, 
                              IImageService imageService, 
                              ClaimsPrincipal principal)
        {
            _context = context;
            _storhService = storhService;
            _imageService = imageService;
            _principal = principal;
        }

        [Route("v1/item/getitemsingroup")]
        [Authorize]
        [HttpGet("{groupId, limit, offset, search}")]
        public IActionResult GetItemsInGroup(int groupId, int limit = 0, int offset = 0, string search = "", bool isShoppingList = false)
        {
            
            if (search == null)
                search = "";

            try
            {
                var sub = _principal.Claims.Where(c => c.Type.Equals("sub")).Select(x => x.Value).FirstOrDefault();
                var items = _storhService.GetItemsInGroup(groupId, sub, isShoppingList);
                return new OkObjectResult(new StorHApiResponse(items));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/item/updateitemcount")]
        [Authorize]
        [HttpPost("{groupId, itemId, count}")]
        public IActionResult UpdateItemCount(int groupId, int itemId, int count)
        {
            try
            {
                var sub = _principal.Claims.Where(c => c.Type.Equals("sub")).Select(x => x.Value).FirstOrDefault();
                var groups = _storhService.GetGroups(sub);
                var allowedToAdd = groups.Select(x => x.id).ToList().IndexOf(groupId) != -1;

                if (!allowedToAdd)
                    return new BadRequestObjectResult(new StorHApiResponse("You are not a part of specified group"));

                _storhService.UpdateItemCount(groupId, itemId, count);
                return new OkObjectResult(new StorHApiResponse());
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/item/barcodesearch")]
        [Authorize]
        [HttpGet("{code}")]
        public IActionResult BarcodeSearch(string code)
        {
            try
            {
                var item = _storhService.BarcodeSearch(code);
                return new OkObjectResult(new StorHApiResponse(item));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Route("v1/item/register")]
        [Authorize]
        [HttpPost("{item}")]
        public IActionResult Register([FromBody] RegItemDTO item)
        {
            try
            {
                _storhService.RegItem(item);
                return new OkObjectResult(new StorHApiResponse());
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException + " " + ex.StackTrace));
            }
        }

        [Authorize]
        [Route("v1/item/uploadimage")]
        [HttpPost("{file}")]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            try
            {
                if (file != null)
                {
                    var itemId = _context.HttpContext.Request.Headers["itemId"].ToString();
                    var itemIdParsed = Int32.Parse(itemId);
                    var claims = ClaimsHelper.GetClaims(_principal);
                    

                    if (itemIdParsed < 0 || claims == null)
                        return new BadRequestObjectResult("itemId missing");

                    await _imageService.storeImage(file, itemIdParsed);

                    return new OkObjectResult(new StorHApiResponse("Image saved"));
                }
                else
                    return new BadRequestObjectResult(new StorHApiResponse("No file provided"));
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new StorHApiResponse(ex.Message + " " + ex.InnerException));
            }
        }

    }
}
